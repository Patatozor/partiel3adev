<?php
spl_autoload_register(function ($class) {
    require 'application/config/config_autoloader.php';
    $i=0;
    $lenght = count($array_of_paths)-1;
    while(!file_exists($array_of_paths[$i].$class.'.php')&&$i<$lenght){
        $i++;
    }
    if(file_exists($array_of_paths[$i].$class.'.php')){
        include $array_of_paths[$i].$class.'.php';
    }
});