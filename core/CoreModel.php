<?php

/**
 * Created by PhpStorm.
 * User: Rémi
 * Date: 24/11/2015
 * Time: 11:48
 */
class CoreModel extends Core
{
    const DEFAULT_ORDER = "ASC";
    protected $_pdo;
    protected $_possible_order = array("DESC","ASC","desc","asc");
    protected $_table;
    protected $_id_name;
    protected $_class;

    function __construct()
    {
        try {
            $dns = 'mysql:host=' . HOST . ';dbname=' . DBNAME;
            $utilisateur = DBLOGIN;
            $motDePasse = DBPASSWORD;
            $options = array(
                PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
            );
            $this->_pdo = new PDO($dns, $utilisateur, $motDePasse, $options);
            $this->_class = get_class($this);
            $this->_table = DBPREFIXE . lcfirst ( str_replace ( "Model" , "" ,$this->_class ) );
            $this->_id_name = "id".$this->_table;
            return true;
        } catch (Exception $e) {
            //return false;
            die("Pb connection");
        }
    }

    /**
     * @param array $params
     * de la forme $params = array(
     *      'orderBy' => array(
     *          'col1' => 'desc',
     *          'col2' => 'asc',
     *          'col3' => 'DESC'
     *      ),
     *      'offset' => 5,
     *      'limit' => 10
     * )
     * @return array|string
     */
    function readAll(array $params)

    {
        $query = '
            SELECT *
            FROM '.$this->_table;
        if(isset($params['orderBy'])&&is_array($params['orderBy'])){
            $query .= ' ORDER BY ';
            foreach ($params['orderBy'] as $key => $value) {
                if($key ==''&&is_numeric($key)){
                    $key = $value;
                    $value = self::DEFAULT_ORDER;
                }elseif(!in_array($value,$this->_possible_order)){
                    $value = self::DEFAULT_ORDER;
                }
                $query .= $key . " " . $value . ", ";
            }
                $query = substr($query, 0, -2);
        }else{
            throw new InvalidArgumentException("La case order doit être un array");
        }
        if(isset($params['offset'])&& !is_array($params['offset']) && is_numeric($params['offset']) && $params['offset']>=0) {
            $offset = $params['offset'];
        }else {
            $offset = OFFSET_DEFAULT;
        }
        if(isset($params['limit']) && !is_array($params['limit']) && is_numeric($params['limit']) && $params['limit']>0) {
            $limit = $params['limit'];
        }else {
            $limit = LIMIT_DEFAULT;
        }
        if(isset($offset)&&isset($limit)) {
            $query .= ' LIMIT ' . $offset . ',' . $limit;
        }
        try {
            $requete = $this->_pdo->query($query);
            $results = $requete->fetchAll();
            $requete->closeCursor();
            return $results;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * @param $id
     * @return array|string
     */
    function readById($id){
        $query = '
            SELECT *
            FROM '.$this->_table.'
            WHERE '.$this->_id_name.' = :id ';
        try {
            $query = $this->_pdo->prepare($query);
            $query->bindValue(':id',$id,PDO::PARAM_INT);
            $query->execute();
            $results = $query->fetchAll();
            $query->closeCursor();
            return $results;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    function countColWithValue($col,$value,$paramtype){
        try{
            $query = 'SELECT COUNT('.$col.')
                FROM '.$this->_table.'
                WHERE :'.$col.' = '.$value.' ;';
            $query = $this->_pdo->prepare($query);
            $query->bindValue(':'.$col,$value,($paramtype=='int')?PDO::PARAM_INT:PDO::PARAM_STR);
            $query->execute();
            $result = $query->fetch();
            return $result;
        }catch (Exception $e) {
            return $e->getMessage();
        }
    }


    /**
     * @param $id
     * @return bool|string
     */
    function deleteById($id){

        $query = '
            DELETE
            FROM '.$this->_table.'
            WHERE '.$this->_id_name.' = :id ';
        try {
            $query = $this->_pdo->prepare($query);
            $query->bindValue(':id',$id,PDO::PARAM_INT);
            $query->execute();
            $query->closeCursor();
            return true;
        } catch (Exception $e) {

            return $e->getMessage();
        }
    }

    /**
     * @return bool
     * @throws Exception
     */
    function insert(){
        try{
            $query = 'INSERT INTO '.$this->_table;
            $cols = '';
            $vals = '';
            foreach($this as $key => $value){
                if( (preg_match('#^'.DBPREFIXE.'#' ,$key)  || (preg_match('#^id'.DBPREFIXE.'#', $key) && $key != $this->_id_name)) && $value != null && $value != '' ){
                    $cols .= $key.',';
                    $vals .= "'".$value."',";
                }
            }
            $cols = substr($cols,0,-1);
            $vals = substr($vals,0,-1);
            $query .= ' ('.$cols.') VALUES ('.$vals.')';
            echo $query;
            $_SESSION['queryperso'] = $query;
            $this->_pdo->query($query);
            $lastid= $this->_pdo->lastInsertId();
            return $lastid;

        }catch (Exception $e){
            //echo $e->getMessage();
            throw new Exception($e->getMessage());
        }
    }


    /**
     * @param $query
     * @param array $values
     * @param $querytype
     * @return array|bool|mixed
     */
    function preparedQuery($query,array $values,$querytype){
        try{
            $query = $this->_pdo->prepare($query);
            foreach ($values as $key => $value) {
                $param = ':'.$key;
                if($value == 'int'){
                    $query->bindValue($param,$key,PDO::PARAM_INT);
                }else{
                    $query->bindValue($param,$key,PDO::PARAM_STR);
                }
            }
            $query->execute();
            $query->closeCursor();
            switch ($querytype){
                case 'fetch':
                    $result = $query->fetch();
                    break;
                case 'fetchAll':
                    $result = $query->fetchAll();
                    break;
                case 'update':
                    $result = true;
                    break;
                case 'insert':
                    $result = true;
                    break;
                case 'delete':
                    $result = true;
                    break;
                case 'other':
                    $result = true;
                    break;
                default:
                    $result = false;
            }
            return $result;
        }catch (Exception $e){
            return false;
        }
    }

    /**
     * @param $table_string
     */
    public function setTable($table_string){
        $this->_table = $table_string;
    }

    function update($id){
        try{
            $query = "UPDATE ".$this->_table;
            $query .= " SET ";
            foreach($this as $key => $value){
                if( preg_match('#^'.DBPREFIXE.'#' ,$key ) && $value != null && $value != '' ){
                    $query .= $key." = '".$value."', ";
                }
            }
            $query = substr($query,0,-2);
            $query .= " WHERE ".$this->_id_name." = ".$id;
            $_SESSION['query'] = $query;
            $this->_pdo->query($query);
            return true;
        }catch (Exception $e){
            //echo $e->getMessage();
            throw new Exception("Error during update in database");
        }
    }

    function table_count(){
        try{
            $query = $this->_pdo->query("SELECT COUNT(*) FROM ".$this->_table);
            $count = $query->fetch();
            return $count['0'];
        }catch (Exception $e){
            //throw new Exception($e->getMessage());
            throw new Exception("Error during request to database");
        }
    }
}

