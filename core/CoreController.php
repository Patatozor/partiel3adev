<?php
/**
 * Created by PhpStorm.
 * User: Rémi
 * Date: 24/11/2015
 * Time: 11:47
 */

class CoreController extends Core{
    public $load;
    public $model;

    function __construct()
    {
        $this->load = new CoreView();
        if(isset($_GET['action'])){
            $action = '_'.$_GET['action'];
        }else{
            $action = '_'.DEFAULT_ACTION;
        }
        if(method_exists($this, $action)){
            $this->$action();
        }else{
            define("TITLE_LAYOUT","Cette page n'existe pas");
            $this->load->view("404.php");
        }
    }
}