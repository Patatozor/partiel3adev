<?php
/**
 * Created by PhpStorm.
 * User: Rémi
 * Date: 24/11/2015
 * Time: 11:47
 */

class CoreView extends Core{
    public $js = '';

    function view( $file_name, $data = null , $data2 = null )
    {
        $js = $this->js;
        include 'views/'. $file_name;
    }

    function jsLoader(array $array){
        foreach($array as $jsfile){
            $this->js .= '<script type="text/javascript" src="'.$jsfile.'" ></script>';
        }
    }
}