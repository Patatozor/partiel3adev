<?php
/**
 * Created by PhpStorm.
 * User: Rémi
 * Date: 02/01/2016
 * Time: 22:03
 */

class StringValidator {

    static public function validateMail($string){
            return (isset($string)&&filter_var($string,FILTER_VALIDATE_EMAIL));
    }
}