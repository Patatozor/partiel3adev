<?php
/**
 * Created by PhpStorm.
 * User: Rémi
 * Date: 10/02/2016
 * Time: 10:15
 */

class Paginate {
    private $number_of_pages_displayed = 5;
    private $results_per_page = LIMIT_DEFAULT;
    private $total_number_of_results;
    private $half;
    private $maxpage;
    private $minpage = 1;
    private $page = 1;
    private $i;
    private $j;
    private $class_for_ul = DEFAULT_PAGINATION_UL_CLASS;
    private $class_for_li = DEFAULT_PAGINATION_LI_CLASS;
    private $class_for_li_actual_page = DEFAULT_PAGINATION_LI_CLASS;
    private $firstPageText = "<<";
    private $lastPageText = ">>";
    private $previousPageText = "<";
    private $nextPageText = ">";

    /**
     * @param $number_of_results : le nombre de resultats total
     * @param string $results_per_page : le nombre de resultats par page, par défaut il est défini dans le fichier config
     * @param string $number_of_pages_displayed : le nombre de pages représentées dans la pagination
     * @throws CustomizedException : si le paramètre number_of_results est incorrect (pas un entier), renvoie une erreur
     */
    public function __construct($number_of_results,$results_per_page="",$number_of_pages_displayed=""){
        if(is_int($number_of_results)&&$number_of_results>=0){
            $this->total_number_of_results = $number_of_results;
        }else{
            throw new CustomizedException('Number of results is not an int');
        }
        if($results_per_page!="" && is_int($results_per_page)){
            $this->results_per_page = $results_per_page;
        }
        if($number_of_pages_displayed!="" && is_int($number_of_pages_displayed)){
            $this->number_of_pages_displayed = $number_of_pages_displayed;
        }
        $this->maxpage = (is_int($number_of_results/$results_per_page))?$number_of_results/$results_per_page:(int)($number_of_results/$results_per_page + 1);
        $this->half = (int) $this->number_of_pages_displayed/2;
    }

    /**
     * @param int $number_of_pages_displayed
     */
    public function setNumberOfPagesDisplayed($number_of_pages_displayed)
    {
        $this->number_of_pages_displayed = $number_of_pages_displayed;
    }

    /**
     * @param string $class_for_ul
     */
    public function setClassForUl($class_for_ul)
    {
        $this->class_for_ul = $class_for_ul;
    }

    /**
     * @param string $class_for_li
     */
    public function setClassForLi($class_for_li)
    {
        $this->class_for_li = $class_for_li;
    }

    /**
     * @param string $class_for_li_actual_page
     */
    public function setClassForLiActualPage($class_for_li_actual_page)
    {
        $this->class_for_li_actual_page = $class_for_li_actual_page;
    }

    /**
     * @param string $firstPageText
     */
    public function setFirstPageText($firstPageText)
    {
        $this->firstPageText = $firstPageText;
    }

    /**
     * @param string $lastPageText
     */
    public function setLastPageText($lastPageText)
    {
        $this->lastPageText = $lastPageText;
    }

    /**
     * @param string $previousPageText
     */
    public function setPreviousPageText($previousPageText)
    {
        $this->previousPageText = $previousPageText;
    }

    /**
     * @param string $nextPageText
     */
    public function setNextPageText($nextPageText)
    {
        $this->nextPageText = $nextPageText;
    }

    /**
     * @param int $results_per_page
     */
    public function setResultsPerPage($results_per_page)
    {
        $this->results_per_page = $results_per_page;
    }

    /**
     * @param int $total_number_of_results
     */
    public function setTotalNumberOfResults($total_number_of_results)
    {
        $this->total_number_of_results = $total_number_of_results;
    }

    /**
     * @param int $maxpage
     */
    public function setMaxpage($maxpage)
    {
        $this->maxpage = $maxpage;
    }

    /**
     * @param int $minpage
     */
    public function setMinpage($minpage)
    {
        $this->minpage = $minpage;
    }

    /**
     * @param int $page
     */
    public function setPage($page)
    {
        $this->page = $page;
    }

    /**
     * @return int
     */
    public function getI()
    {
        return $this->i;
    }

    /**
     * @return int
     */
    public function getJ()
    {
        return $this->j;
    }

    /**
     * @return void
     * Génère un i et un j pour une boucle for de la forme:
     * for($i;$i<=$j;$i++)
     */
    private function generateIandJ(){
        if(($this->page - $this->half)>=$this->minpage){
            $this->i = $this->page - $this->half;
        }else{
            $this->i = $this->minpage;
        }
        if(($this->page + $this->half)<=$this->maxpage){
            $this->j = $this->page + $this->half;
        }else{
            $this->j = $this->maxpage;
        }
    }



    /**
     * @return string Renvoie une pagination HTML
     * @param BOOL $withNextPreviousLinks : Si l'on veut que la fonction génère les liens page précédente et page suivante dans la barre de pagination: True par défaut
     * @param BOOL $withFastLinks : Si l'on veut que la fonction génère les liens première page et dernière page dans la barre de pagination: True par défaut
     */
    public function buildPagination($withNextPreviousLinks = true,$withFastLinks = true){
        $this->generateIandJ();
        $pagination = '<ul class="'.$this->class_for_ul.'">';
        if($this->page > $this->minpage){
            if($withFastLinks == true){
                $pagination .= '<li class="'.$this->class_for_li.'"><a href="'.$_SERVER['REQUEST_URI'].'&page='.$this->minpage.'">'.$this->firstPageText.'</a></li>';
            }
            if($withNextPreviousLinks == true){
                $pagination .= '<li class="'.$this->class_for_li.'"><a href="'.$_SERVER['REQUEST_URI'].'&page='.($this->page - 1).'">'.$this->previousPageText.'</a></li>';
            }
        }
        for($i=$this->i;$i<=$this->j;$i++){
            if($i == $this->page){
                $pagination .= '<li class="'.$this->class_for_li_actual_page.'"><a href="#">'.$i.'</a></li>';
            }else{
                $pagination .= '<li class="'.$this->class_for_li.'"><a href="'.$_SERVER['REQUEST_URI'].'&page='.$i.'">'.$i.'</a></li>';
            }
        }

        if($this->page < $this->maxpage){
            if($withNextPreviousLinks == true){
                $pagination .= '<li class="'.$this->class_for_li.'"><a href="'.$_SERVER['REQUEST_URI'].'&page='.($this->page + 1).'">'.$this->nextPageText.'</a></li>';
            }
            if($withFastLinks == true){
                $pagination .= '<li class="'.$this->class_for_li.'"><a href="'.$_SERVER['REQUEST_URI'].'&page='.($this->maxpage).'">'.$this->lastPageText.'</a></li>';
            }
        }
        $pagination .= '</ul>';
        return $pagination;
    }
}