<?php

/**
 * Created by PhpStorm.
 * User: Manu
 * Date: 30/01/2016
 * Time: 23:32
 */
class Notice{

    public static function setFlash($message,$type = 'error'){

        $_SESSION['flash'] = array(
            'message' => $message,
            'type' => $type
        );
    }

    public static function flashFront(){

        if(isset($_SESSION['flash'])){?>

            <div class="<?php echo $_SESSION['flash']['type'];?> callout" data-closable="slide-out-right">
                <h5><?php echo $_SESSION['flash']['message'];?></h5>
                <button class="close-button" aria-label="Dismiss alert" type="button" data-close>
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php unset($_SESSION['flash']);

        }
    }

    public static function flashBack(){

        if(isset($_SESSION['flash'])){?>

            <div role="alert" id="alert" class="<?php echo $_SESSION['flash']['type'];?> ">
                <button type="button" data-dismiss="alert" aria-label="Close" class="close">
                    <span aria-hidden="true" id="alert-close">×</span>
                </button><i class="alert-ico fa fa-fw fa-check"></i>
                <?php echo $_SESSION['flash']['message'];?>
            </div>
            <?php unset($_SESSION['flash']);

        }
    }



}