<?php include_once 'views/header.php';?>
<?php Notice::flashBack(); ?>
    <table class="table">
        <tr>
            <th>Id de l'utilisateur</th>
            <th>Nom de l'utilisateur</th>
            <th>Mail de l'utilisateur</th>
            <th>Catégorie de l'utilisateur</th>
        </tr>
        <?php foreach($data['0'] as $user){ ?>
            <tr>
                <td><?php echo $user['0']; ?></td>
                <td><?php echo $user['users_name']; ?></td>
                <td><?php echo $user['users_mail']; ?></td>
                <td><?php echo $user['categories_name']; ?></td>
                <td><a href="index.php?module=Users&action=modifier&id=<?php echo $user['0']; ?>" class="btn btn-info">Modifier</a> <a href="index.php?module=Users&action=supprimer&id=<?php echo $user['0']; ?>" class="btn btn-danger">Supprimer</a></td>
            </tr>
        <?php } ?>
    </table>

<?php echo $data['1']->buildPagination(); ?>

<?php include_once 'views/footer.php'; ?>