<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title><?php echo TITLE_LAYOUT; ?></title>
    <link rel="stylesheet" href="assets/css/bootstrap.css">
</head>
<body>
<header>
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li><a href="index.php">Home</a></li>
                    <li><a href="index.php?module=Home&action=audit">Liste des audits</a></li>
                    <li><a href="index.php?module=Users&action=creer">Créer un utilisateur</a></li>
                    <li><a href="index.php?module=Users&action=liste">Liste des utilisateurs</a></li>
                    <li><a href="index.php?module=Categories&action=creer">Créer une catégorie</a></li>
                    <li><a href="index.php?module=Categories&action=liste">Liste des catégories</a></li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
</header>
<div class="row">
    <div class="col-md-10 col-md-offset-1">
