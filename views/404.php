<?php include_once (ROOTPATH.'/views/header.php'); ?>

	<div class="container build-page">
		<div class="tableau">
			<div class="tableau-cell">
				<div class="row align-center">
					<div class="small-12 medium-6 large-6 columns">
						<div class="row align-middle">
						</div>
						<div class="row align-center">
							<h1 class="small-12 columns text-center">Oooooops, looks like you've lost your way !</h1>
							<p class="small-12 columns text-center">The page you were looking has been mislaid, sorry.</p>
							<div class="small-4 columns">
								<hr>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

</div>

<?php include_once 'views/footer.php'; ?>