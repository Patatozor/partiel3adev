<?php include_once 'views/header.php'; ?>
<?php Notice::flashBack(); ?>

<?php Notice::flashBack(); ?>
<form method="post" action="index.php?module=Users&action=traitement_ajout">
    <div class="col-md-6">
        <label class="col-md-4" for="users_mail">Adresse mail</label>
        <input class="col-md-7 col-md-offset-1" name="users_mail" id="users_mail" type="email" required="required" value="">
    </div>
    <div class="col-md-6">
        <label class="col-md-4" for="users_name">Nom</label>
        <input class="col-md-7 col-md-offset-1" name="users_name" id="users_name" type="text" required="required" value="">
    </div>
    <br><br>
    <div class="col-md-6">
        <label class="col-md-4" for="users_password">Mot de passe</label>
        <input class="col-md-7 col-md-offset-1" name="users_password" id="users_password" type="password" required="required" value="">
    </div>
    <div class="col-md-6">
        <label class="col-md-4" for="categories_idcategories">Catégorie</label>
        <select class="col-md-7 col-md-offset-1" name="categories_idcategories" id="categories_idcategories">
            <?php foreach($data as $option){ ?>
                <option value="<?php echo $option['0']; ?>"><?php echo $option['1']; ?></option>
            <?php } ?>
        </select>
    </div>
    <br>
    <br>
    <div class="col-md-12 text-center">
        <input type="submit" class="btn btn-success col-md-2 col-md-offset-5" value="Valider">
    </div>
</form>

<?php include_once 'views/footer.php'; ?>