<?php include_once 'views/header.php'; ?>
<table class="table">
    <tr>
        <th>Id de l'utilisateur</th>
        <th>Mail de l'utilisateur</th>
        <th>Nom de l'utilisateur</th>
        <th>Action</th>
        <th>Date</th>
    </tr>
    <?php foreach($data['0'] as $audit){ ?>
        <tr>
            <td><?php echo $audit['users_audit_user_id']; ?></td>
            <td><?php echo $audit['users_audit_user_mail']; ?></td>
            <td><?php echo $audit['users_audit_user_name']; ?></td>
            <td><?php echo $audit['users_audit_action']; ?></td>
            <td><?php echo $audit['users_audit_date']; ?></td>
        </tr>
    <?php } ?>
</table>

<?php echo $data['1']->buildPagination(); ?>

<?php include_once 'views/footer.php'; ?>