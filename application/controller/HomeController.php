<?php
/**
 * Created by PhpStorm.
 * User: Manu
 * Date: 05/12/2015
 * Time: 16:43
 */
class HomeController extends AppController
{
    protected function _home()
    {
        require_once 'services/Notice.php';
        define("TITLE_LAYOUT", "Accueil Partiel");
        $this->load->view('home.php');
        $_SESSION['flash'] = array();
        unset($_SESSION['flash']);
    }

    protected function _audit(){
        try{
            $audit = new Users_auditModel();
            $count = $audit->table_count();
            $paginate = new Paginate((int)$count,LIMIT_DEFAULT,5);
            if(isset($_GET['page']) && is_int($_GET['page'])){
                $offset = ($_GET['page']-1)*LIMIT_DEFAULT;
                $paginate->setPage($_GET['page']);
            }else{
                $offset = 0;
                $paginate->setPage(1);
            }
            $data['0'] = $audit->readAll(array(
                   'orderBy' => array('idusers_audit' => 'desc'),
                   'offset' => $offset,
                   'limit' => LIMIT_DEFAULT));
            define("TITLE_LAYOUT","Affichage de la table d'audit");
            $data['1'] = $paginate;
            $this->load->view('audit.php',$data);
        }catch (Exception $e){
            Notice::setFlash($e->getMessage());
            header('location:index.php');
            echo $e->getMessage();
        }

    }

}