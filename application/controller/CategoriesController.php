<?php
/**
 * Created by PhpStorm.
 * User: Rémi
 * Date: 23/02/2016
 * Time: 10:17
 */

class CategoriesController extends AppController{
    protected function _creer()
    {
        try {
            require_once 'services/Notice.php';
            define("TITLE_LAYOUT", "Créer une catégorie");
            $this->load->view('add_categorie.php');
            $_SESSION['flash'] = array();
            unset($_SESSION['flash']);
        } catch (Exception $e) {
            Notice::setFlash($e->getMessage());
            header('location:index.php');
        }
    }

    protected function _traitement_ajout(){
        try{
            if(isset($_POST['categories_name'])){
                $categorie = new CategoriesModel();
                $categorie->setCategoriesName($_POST['categories_name']);
                $categorie->insert();
                Notice::setFlash('Votre catégorie a bien été ajoutée');
                header('location:index.php?module=Categories&action=liste');
            }else{
                Notice::setFlash('Veuillez entrer un nom de catégorie');
                header('location:index.php?module=Categories&action=creer');
            }
        }catch (Exception $e){
            Notice::setFlash($e->getMessage());
            header('location:index.php?module=Categories&action=creer');
        }
    }

    protected function _liste(){
        try{
            require_once 'services/Notice.php';
            $categories = new CategoriesModel();
            $count = $categories->table_count();
            $paginate = new Paginate((int)$count,LIMIT_DEFAULT,5);
            if(isset($_GET['page']) && is_int($_GET['page'])){
                $offset = ($_GET['page']-1)*LIMIT_DEFAULT;
                $paginate->setPage($_GET['page']);
            }else{
                $offset = 0;
                $paginate->setPage(1);
            }
            $data['0'] = $categories->readAll(array(
                'orderBy' => array('idcategories' => 'asc'),
                'offset' => $offset,
                'limit' => LIMIT_DEFAULT));
            define("TITLE_LAYOUT", "Liste des catégorie");
            $data['1'] = $paginate;
            $this->load->view('liste_categories.php',$data);
            $_SESSION['flash'] = array();
            unset($_SESSION['flash']);
        }catch (Exception $e){
            Notice::setFlash($e->getMessage());
            header('location:index.php');
        }
    }
}