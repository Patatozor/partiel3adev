<?php
/**
 * Created by PhpStorm.
 * User: Rémi
 * Date: 23/02/2016
 * Time: 10:18
 */

class UsersController extends AppController{
    protected function _creer(){
        try{
            require_once 'services/Notice.php';
            $categories = new CategoriesModel();
            $data = $categories->readAll(array(
                'orderBy' => array('idcategories' => 'desc'),
                OFFSET_DEFAULT,
                30));
            define("TITLE_LAYOUT", "Créer un utilisateur");
            $this->load->view('add_user.php',$data);
            $_SESSION['flash'] = array();
            unset($_SESSION['flash']);
        }catch (Exception $e){
            Notice::setFlash($e->getMessage());
            header('location:index.php');
        }

    }

    protected function _traitement_ajout(){
        try{
            if(isset($_POST['users_mail'])){
                $user = new UsersModel();
                $user->setUsersMail($_POST['users_mail']);
                $user->setUsersName($_POST['users_name']);
                $user->setUsersPassword($_POST['users_password']);
                $user->setCategoriesIdcategories($_POST['categories_idcategories']);
                $user->insert();
                Notice::setFlash('L\'utilisateur a bien été ajoutée');
                header('location:index.php?module=Users&action=liste');
            }else{
                Notice::setFlash('Veuillez entrer une adresse mail');
                header('location:index.php?module=Users&action=creer');
            }
        }catch (Exception $e){
            Notice::setFlash($e->getMessage());
            header('location:index.php?module=Users&action=creer');
        }
    }

    protected function _liste(){
        try{
            require_once 'services/Notice.php';
            $users = new AppModel();
            $count = $users->count_users();
            $paginate = new Paginate((int)$count,LIMIT_DEFAULT,5);
            if(isset($_GET['page']) && is_int($_GET['page'])){
                $offset = ($_GET['page']-1)*LIMIT_DEFAULT;
                $paginate->setPage($_GET['page']);
            }else{
                $offset = 0;
                $paginate->setPage(1);
            }
            $data['0'] = $users->readView($offset,LIMIT_DEFAULT);
            define("TITLE_LAYOUT", "Liste des Utilisateurs");
            $data['1'] = $paginate;
            $this->load->view('liste_users.php',$data);
            $_SESSION['flash'] = array();
            unset($_SESSION['flash']);
        }catch (Exception $e){
            Notice::setFlash($e->getMessage());
            header('location:index.php');
        }
    }

    public function _supprimer(){
        try{
            if(isset($_GET['id'])&&is_numeric($_GET['id'])){
                $user = new UsersModel();
                $user->deleteById($_GET['id']);
                Notice::setFlash("L'utilisateur a bien été supprimé");
                header('location:index.php?module=Users&action=liste');
            }else{
                header('location:index.php?module=Users&action=liste');
            }
        }catch (Exception $e){
            Notice::setFlash($e->getMessage());
            header('location:index.php?module=Users&action=liste');
            echo $e->getMessage();
        }
    }

    public function _modifier(){

    }
}