<?php
require 'config/config.php';

if (!isset($_GET['module'])) {
    $module = DEFAULT_MODULE;
} else {
    $module = $_GET['module'];
}
$file = $module . "Controller.php";
if (!file_exists("application/controller/" . $file)) {
    require "views/404.php";

}  else {
    $controller = $module . "Controller";
    new $controller();
}


