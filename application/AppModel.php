<?php
/**
 * Created by PhpStorm.
 * User: Rémi
 * Date: 24/11/2015
 * Time: 12:36
 */

class AppModel extends CoreModel{
    public function readView($offset,$limit){
        try{
            $query = $this->_pdo->prepare("
              SELECT * FROM par_users_of_categorie
              LIMIT :offset,:limit
            ");
            $query->bindValue(':offset',$offset,PDO::PARAM_INT);
            $query->bindValue(':limit',$limit,PDO::PARAM_INT);
            $query->execute();
            $results = $query->fetchAll();
            return $results;
        }catch (Exception $e){
            //throw new CustomizedException($e->getMessage());
            throw new CustomizedException('Erreur lors de la requête');
        }
    }

    public function count_users(){
        try{
            $query = $this->_pdo->query("SELECT COUNT(*) FROM par_users_of_categorie");
            $count = $query->fetch();
            return $count['0'];
        }catch (Exception $e){
            //throw new Exception($e->getMessage());
            throw new Exception("Error during request to database");
        }
    }
}