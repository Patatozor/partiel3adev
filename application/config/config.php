<?php
// Limite et offset pour d'éventuelles listes de données
define('OFFSET_DEFAULT', 0);
define('LIMIT_DEFAULT', 10);
//Chemin absolu de l'application
define('ROOTPATH', dirname(dirname(__DIR__)));
// Préfixe de table de la base de données
define('DBPREFIXE', 'par_');
// Nom de la variable de session
define('SESSION_NAME', 'partiel3ADEV');
// Type du serveur
define('SERVER', 'TEST');

//pagination
define('DEFAULT_PAGINATION_UL_CLASS','pagination');
define('DEFAULT_PAGINATION_LI_CLASS','');

//Partie du site par défaut
define('SEL','!sdf5aZe:;459d?');

//Test du type de serveur
if (SERVER === "DEV") {

    /**
     * Constantes spécifiques pour le serveur de dev
     */

    define('DEBUG', true);
    define('RUN', 'NORMAL');
    define('HOST', 'localhost');
    define('DBNAME', 'fumeron');
    define('DBLOGIN', 'root');
    define('DBPASSWORD', '');
    define('DEFAULT_MODULE', 'Home');
    define('DEFAULT_ACTION', 'home');
    define('SITE_ROOT','localhost/partielPHPSQL/');


} elseif (SERVER === "TEST") {

    /**
     * Constantes spécifiques pour le serveur de test
     */

    define('DEBUG', true);
    define('RUN', 'NORMAL');
    define('HOST', 'dev.etudiant-eemi.com');
    define('DBNAME', 'fumeron');
    define('DBLOGIN', 'fumeron');
    define('DBPASSWORD', '140742');
    define('DEFAULT_MODULE', 'Home');
    define('DEFAULT_ACTION', 'home');
    define('SITE_ROOT','fumeron.etudiant-eemi.com/perso/partielPHPSQL/');

} elseif (SERVER === 'PROD') {

    /**
     * Constantes spécifiques pour le serveur de prod
     */

    define('DEBUG', false);
    define('RUN', 'NORMAL');
    define('HOST', 'dev.etudiant-eemi.com');
    define('DBNAME', 'fumeron');
    define('DBLOGIN', 'fumeron');
    define('DBPASSWORD', '140742');
    define('DEFAULT_MODULE', 'Home');
    define('DEFAULT_ACTION', 'home');
    define('SITE_ROOT','fumeron.etudiant-eemi.com/perso/partielPHPSQL/');

}

//Gestion des erreurs PHP pour être indépendant du php.ini

if (defined('DEBUG') && DEBUG) {
    ini_set('display_errors', 1);
    error_reporting(E_ALL);
} else {

    ini_set('display_errors', 0);
    error_reporting(0);
}
