<?php
/**
 * Created by PhpStorm.
 * User: Rémi
 * Date: 23/02/2016
 * Time: 10:57
 */

class Users_auditModel extends AppModel{
    private $idusers_audit;
    private $users_audit_action;
    private $users_audit_date;
    private $users_audit_user_id;
    private $users_audit_user_mail;
    private $users_audit_user_name;

    /**
     * @return mixed
     */
    public function getIdusersAudit()
    {
        return $this->idusers_audit;
    }

    /**
     * @param mixed $idusers_audit
     */
    public function setIdusersAudit($idusers_audit)
    {
        $this->idusers_audit = $idusers_audit;
    }

    /**
     * @return mixed
     */
    public function getUsersAuditAction()
    {
        return $this->users_audit_action;
    }

    /**
     * @param mixed $users_audit_action
     */
    public function setUsersAuditAction($users_audit_action)
    {
        $this->users_audit_action = $users_audit_action;
    }

    /**
     * @return mixed
     */
    public function getUsersAuditDate()
    {
        return $this->users_audit_date;
    }

    /**
     * @param mixed $users_audit_date
     */
    public function setUsersAuditDate($users_audit_date)
    {
        $this->users_audit_date = $users_audit_date;
    }

    /**
     * @return mixed
     */
    public function getUsersAuditUserId()
    {
        return $this->users_audit_user_id;
    }

    /**
     * @param mixed $users_audit_user_id
     */
    public function setUsersAuditUserId($users_audit_user_id)
    {
        $this->users_audit_user_id = $users_audit_user_id;
    }

    /**
     * @return mixed
     */
    public function getUsersAuditUserMail()
    {
        return $this->users_audit_user_mail;
    }

    /**
     * @param mixed $users_audit_user_mail
     */
    public function setUsersAuditUserMail($users_audit_user_mail)
    {
        $this->users_audit_user_mail = $users_audit_user_mail;
    }

    /**
     * @return mixed
     */
    public function getUsersAuditUserName()
    {
        return $this->users_audit_user_name;
    }

    /**
     * @param mixed $users_audit_user_name
     */
    public function setUsersAuditUserName($users_audit_user_name)
    {
        $this->users_audit_user_name = $users_audit_user_name;
    }


}