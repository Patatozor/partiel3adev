<?php
/**
 * Created by PhpStorm.
 * User: Rémi
 * Date: 23/02/2016
 * Time: 10:56
 */

class UsersModel extends AppModel{
    private $idusers;
    private $users_mail;
    private $users_name;
    private $users_password;
    private $categories_idcategories;

    /**
     * @return mixed
     */
    public function getIdusers()
    {
        return $this->idusers;
    }

    /**
     * @param mixed $idusers
     */
    public function setIdusers($idusers)
    {
        $this->idusers = $idusers;
    }

    /**
     * @return mixed
     */
    public function getUsersMail()
    {
        return $this->users_mail;
    }

    /**
     * @param mixed $users_mail
     */
    public function setUsersMail($users_mail)
    {
        if(StringValidator::validateMail($users_mail)){
            $this->users_mail = $users_mail;
        }else{
            throw new CustomizedException('Adresse mail invalide');
        }
    }

    /**
     * @return mixed
     */
    public function getUsersName()
    {
        return $this->users_name;
    }

    /**
     * @param mixed $users_name
     */
    public function setUsersName($users_name)
    {
        $this->users_name = $users_name;
    }

    /**
     * @return mixed
     */
    public function getUsersPassword()
    {
        return $this->users_password;
    }

    /**
     * @param mixed $users_password
     */
    public function setUsersPassword($users_password)
    {
        $this->users_password = md5($users_password);
    }

    /**
     * @return mixed
     */
    public function getCategoriesIdcategories()
    {
        return $this->categories_idcategories;
    }

    /**
     * @param mixed $categories_idcategories
     */
    public function setCategoriesIdcategories($categories_idcategories)
    {
        if(is_numeric($categories_idcategories)){
            $this->categories_idcategories = $categories_idcategories;
        }else{
            throw new CustomizedException('Catégorie invalide');
        }

    }

    public function insert(){
        try{
            $query = 'INSERT INTO '.$this->_table.'
            SET
            users_mail = "'.$this->users_mail.'",
            users_name = "'.$this->users_name.'",
            users_password = "'.$this->users_password.'",
            categories_idcategories = '.$this->categories_idcategories;
            $this->_pdo->query($query);
            return true;
        }catch (Exception $e){
            //echo $e->getMessage();
            throw new Exception($e->getMessage());
        }
    }

    public function deleteById($id){
        $query = '
            DELETE
            FROM '.$this->_table.'
            WHERE idusers = :id ';
        try {
            $query = $this->_pdo->prepare($query);
            $query->bindValue(':id',$id,PDO::PARAM_INT);
            $query->execute();
            $query->closeCursor();
            return true;
        } catch (Exception $e) {
            throw new CustomizedException('Erreur lors de la suppression');
            //throw new CustomizedException($e->getMessage());
        }
    }
}