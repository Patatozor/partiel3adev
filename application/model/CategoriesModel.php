<?php
/**
 * Created by PhpStorm.
 * User: Rémi
 * Date: 23/02/2016
 * Time: 10:55
 */

class CategoriesModel extends AppModel{
    public $idcategories;
    public $categories_name;

    /**
     * @return mixed
     */
    public function getIdcategories()
    {
        return $this->idcategories;
    }

    /**
     * @param mixed $idcategories
     */
    public function setIdcategories($idcategories)
    {
        $this->idcategories = $idcategories;
    }

    /**
     * @return mixed
     */
    public function getCategoriesName()
    {
        return $this->categories_name;
    }

    /**
     * @param mixed $categories_name
     */
    public function setCategoriesName($categories_name)
    {
        $this->categories_name = $categories_name;
    }

    public function insert(){
        try{
            $query = 'INSERT INTO '.$this->_table.' SET categories_name = "'.$this->categories_name.'"';
            $this->_pdo->query($query);
            return true;
        }catch (Exception $e){
            //echo $e->getMessage();
            throw new Exception($e->getMessage());
        }
    }

}